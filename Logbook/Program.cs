﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logbook
{
    class Program
    {
        static void Main(string[] args)
        {
            Registration registration = new Registration();
            ShowAll showAll = new ShowAll();

            Console.WriteLine("Choose function:");
            Console.WriteLine("{0}. \t{1}", 1, "Add new visitor");
            Console.WriteLine("{0}. \t{1}", 2, "Show all visitors");
            Console.WriteLine("{0}. \t{1}", 3, "Edit exit time");
            int key = int.Parse(Console.ReadLine());
            switch(key)
            {
                case 1: registration.Add();
                    break;
                case 2: showAll.Show();
                    break;
                case 3:
                    showAll.Show();
                    Console.WriteLine("Choose number");
                    int number = int.Parse(Console.ReadLine());
                    registration.Out(number);
                    break;
                default: 
                    break;
            }
            Console.Read();
        }
    }
}
