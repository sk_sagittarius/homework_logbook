﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logbook
{
    public class Registration
    {
        public void Add()
        {
            using (var context = new LogbookContext())
            {
                Journal journal = new Journal();
                Console.WriteLine("Full Name");
                journal.FullName = Console.ReadLine();
                Console.WriteLine("Identity card");
                journal.IdentityCard = Console.ReadLine();
                Console.WriteLine("Visit purpose");
                journal.VisitPurpose = Console.ReadLine();

                context.Journals.Add(journal);
                context.SaveChanges();
            }
        }

        public void Out(int _number)
        {
            using (var context = new LogbookContext())
            {
                Journal journal = new Journal();
                var journalList = context.Journals.ToList();
                if (journalList.ElementAt(_number - 1).IsOut == false)
                {
                    journalList.ElementAt(_number - 1).IsOut = true;
                    journalList.ElementAt(_number - 1).DateOut = DateTime.Now;
                }
                else
                {
                    Console.WriteLine("Error");
                }
                context.SaveChanges();
            }
        }
    }
}
