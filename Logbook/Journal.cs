﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logbook
{
    public class Journal
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime DateIn { get; set; } = DateTime.Now;
        public string FullName { get; set; }
        public string IdentityCard { get; set; }
        public string VisitPurpose { get; set; }
        public bool IsOut { get; set; } = false;
        public DateTime? DateOut { get; set; } = null;

    }
}
