﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logbook
{
    public class ShowAll
    {
        public void Show()
        {
            using (var context = new LogbookContext())
            {
                Journal journal = new Journal();
                var journalList = context.Journals.ToList();
                Console.WriteLine("{0}. {1}  \t\t{2}  \t{3}  \t{4}  \t{5}", "N", "Date, time in", "Full name", "Identity card", "Visit purpose", "Date, time out");
                var _dateOut = "is here";
                for (int i = 0; i < journalList.Count; i++)
                {
                    if (journalList.ElementAt(i).DateOut == null)
                    {
                        Console.WriteLine("{0}. {1}  \t{2}  \t{3}  \t{4}  \t\t{5}", i + 1, journalList.ElementAt(i).DateIn, journalList.ElementAt(i).FullName,
                            journalList.ElementAt(i).IdentityCard, journalList.ElementAt(i).VisitPurpose, _dateOut);
                    }
                    else
                    {
                        Console.WriteLine("{0}. {1}  \t{2}  \t{3}  \t{4}  \t\t{5}", i + 1, journalList.ElementAt(i).DateIn, journalList.ElementAt(i).FullName,
                            journalList.ElementAt(i).IdentityCard, journalList.ElementAt(i).VisitPurpose, journalList.ElementAt(i).DateOut);
                    }
                }
            }
        }
    }
}
